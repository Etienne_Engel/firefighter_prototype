#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <analogWrite.h>
#include <ros.h>
#include <std_msgs/Byte.h>
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/Bool.h>

#include "Adafruit_VL53L0X.h"

ros::NodeHandle nh;

std_msgs::Byte single_android_cmd;
ros::Publisher android_cmd("android_cmd", &single_android_cmd);

std_msgs::Int16MultiArray debug_array;

String  DATA;
String data_value;
const byte buffSize = 30;
unsigned char inputBuffer[buffSize];
byte bytesRecvd = 0;
boolean data_received = false;

//Relay and motor pump
const int RELAY_PIN = 33;

// Motor A connections
int enA = 15;
int in1 = 13;
int in2 = 12;
// Motor B connections
int enB = 25;
int in3 = 26;
int in4 = 27;

// Motor Speed Values - Start at zero
 
int MotorSpeed1 = 0;
int MotorSpeed2 = 0;
 
// Joystick Values - Start at 512 (middle position)
 
int pad_x_pos;
int pad_y_pos; 



//==================================================================
// TOF and Buzzer parameters
//==================================================================

Adafruit_VL53L0X lox = Adafruit_VL53L0X();
const int buzzer = 17; // Buzzer to arduino pin 9
float ratio = 1000/600; // Ratio distance - sound
bool forward_disabled = false;
int freq = 1000;
int channel = 0;
int resolution = 8;

void stop_platform(){
  analogWrite(enA, 0);
  analogWrite(enB, 0);
  }

void platform_control(const std_msgs::Int16MultiArray& data_msg){
 // Read the Joystick X and Y positions
  data_received = true;
  //debug_array = data_msg;
  if (data_msg.data[4] != 1){
  pad_x_pos = data_msg.data[2];
  pad_y_pos = data_msg.data[3];
  // Determine if this is a forward or backward motion
  // Do this by reading the Verticle Value
  // Apply results to MotorSpeed and to Direction

  
  if (pad_y_pos < data_msg.data[1]-10)
  { 
    if (forward_disabled == false){
    // This is Backward
 
    // Set Motor A backward
 
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
 
    // Set Motor B backward
 
    digitalWrite(in3, HIGH);
    digitalWrite(in4, LOW);
 
    //Determine Motor Speeds
 
    // As we are going backwards we need to reverse readings
 
    pad_y_pos = pad_y_pos - data_msg.data[1]-10; // This produces a negative number
    pad_y_pos = pad_y_pos * -1;  // Make the number positive
 
    MotorSpeed1 = map(pad_y_pos, 0, data_msg.data[1]-10, 0, 255);
    MotorSpeed2 = map(pad_y_pos, 0, data_msg.data[1]-10, 0, 255);
   }
       else{
    // This is Stopped
 
    MotorSpeed1 = 0;
    MotorSpeed2 = 0; 
    }
 
  }
  else if (pad_y_pos > data_msg.data[1]+10)
  {
    // This is Forward


    
      // Set Motor A forward
   
      digitalWrite(in1, LOW);
      digitalWrite(in2, HIGH);
   
      // Set Motor B forward
   
      digitalWrite(in3, LOW);
      digitalWrite(in4, HIGH);
   
      //Determine Motor Speeds
   
      MotorSpeed1 = map(pad_y_pos, data_msg.data[1]+10, 253, 0, 255);
      MotorSpeed2 = map(pad_y_pos, data_msg.data[1]+10, 253, 0, 255); 
    
  }
  
  else
  {
    // This is Stopped
 
    MotorSpeed1 = 0;
    MotorSpeed2 = 0; 
 
  }
  
  // Now do the steering
  // The Horizontal position will "weigh" the motor speed
  // Values for each motor
 
  if (pad_x_pos < data_msg.data[0]-10)
  {
    // Move Left
 
    // As we are going left we need to reverse readings
 
    pad_x_pos = pad_x_pos - data_msg.data[0]-10; // This produces a negative number
    pad_x_pos = pad_x_pos * -1;  // Make the number positive
 
    // Map the number to a value of 255 maximum
 
    pad_x_pos = map(pad_x_pos, 0, data_msg.data[0]-10, 0, 255);
        
 
    MotorSpeed1 = MotorSpeed1 + pad_x_pos;
    MotorSpeed2 = MotorSpeed2 - pad_x_pos;
 
    // Don't exceed range of 0-255 for motor speeds
 
    if (MotorSpeed1 < 0)MotorSpeed1 = 0;
    if (MotorSpeed2 > 255)MotorSpeed2 = 255;
 
  }
  else if (pad_x_pos > data_msg.data[0]+10)
  {
    // Move Right
 
    // Map the number to a value of 255 maximum
 
    pad_x_pos = map(pad_x_pos, data_msg.data[0]+10, 253, 0, 255);
        
 
    MotorSpeed1 = MotorSpeed1 - pad_x_pos;
    MotorSpeed2 = MotorSpeed2 + pad_x_pos;
 
    // Don't exceed range of 0-255 for motor speeds
 
    if (MotorSpeed1 > 255)MotorSpeed1 = 255;
    if (MotorSpeed2 < 0)MotorSpeed2 = 0;      
 
  }
 
 
  // Adjust to prevent "buzzing" at very low speed
 
  if (MotorSpeed1 < 8)MotorSpeed1 = 0;
  if (MotorSpeed2 < 8)MotorSpeed2 = 0;
 
  // Set the motor speeds
 
  analogWrite(enA, MotorSpeed1);
  analogWrite(enB, MotorSpeed2);
  }
  
  else
  {
    stop_platform();
    }
 
}

void pump_controller(const std_msgs::Bool& is_firing){
  if (is_firing.data == true){
    digitalWrite(RELAY_PIN, HIGH);
    }
  else if (is_firing.data == false){
    digitalWrite(RELAY_PIN, LOW);
    }
  
  }



ros::Subscriber<std_msgs::Int16MultiArray> sub_wheel("wheel_base_cmd", &platform_control );
ros::Subscriber<std_msgs::Bool> sub_shoot("shoot_cmd", &pump_controller );

#define SERVICE_UUID        "fc7251c4-3f3e-11ec-9bbc-0242ac130002"
#define CHARACTERISTIC_UUID "0fe206a0-3f3f-11ec-9bbc-0242ac130002"


class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();
      if (value.length() > 0) {
        data_value = "";
        for (int i = 0; i < value.length(); i++){
          data_value = data_value + value[i];
        }
      }
        unsigned char byte_from_app[data_value.length() + 1];
        data_value.getBytes(byte_from_app,data_value.length() + 1);
        single_android_cmd.data = *byte_from_app;
        android_cmd.publish( &single_android_cmd );
        //nh.spinOnce();
      }
      
    
};

void setup() {

  if (!lox.begin()) {
    //Serial.println(F("Failed to boot VL53L0X"));
    while(1);
  }
  // power 
  //Serial.println(F("VL53L0X API Simple Ranging example\n\n"));

  pinMode(buzzer, OUTPUT); // Set buzzer - pin 9 as an output
  
  //----------------------------------------------------
  
  DATA = "";
  pinMode(RELAY_PIN, OUTPUT);

  // Set all the motor control pins to outputs
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  
  // Turn off motors - Initial state
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  
  delay(2000);
  
  // Turn on motors
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  
  BLEDevice::init("ESP32");
  BLEServer *pServer = BLEDevice::createServer();

  BLEService *pService = pServer->createService(SERVICE_UUID);

  BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID,
                                         BLECharacteristic::PROPERTY_READ |
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  pCharacteristic->setCallbacks(new MyCallbacks());
  pCharacteristic->setValue("INIT");
  pService->start();

  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->start();

  nh.initNode();
  nh.advertise(android_cmd);
  nh.subscribe(sub_wheel);
  nh.subscribe(sub_shoot);
}

void loop() {
  VL53L0X_RangingMeasurementData_t measure;
    
  //Serial.print("Reading a measurement... ");
  lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
  if (measure.RangeStatus != 4) {  // phase failures have incorrect data
    if (measure.RangeMilliMeter < 600){
      digitalWrite(buzzer, HIGH); // Send 1KHz sound signal...
      delay(ratio*measure.RangeMilliMeter);        // ...for 1 sec
      delay(100);
      digitalWrite(buzzer, LOW);     // Stop sound...
      delay(ratio*measure.RangeMilliMeter);        // ...for 1sec
      
      if (measure.RangeMilliMeter < 100){
        forward_disabled = true;
      }

      else{
        forward_disabled = false;
      }
    }

    else{
      forward_disabled = false;
    }
    
  }

  else{
    forward_disabled = false;
  }
  
  nh.spinOnce();
}
