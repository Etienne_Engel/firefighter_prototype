#!/usr/bin/env python3

import rospy
from std_msgs.msg import Byte, Int16MultiArray, Bool


class FirefighterPrototype():

    def __init__(self):
        rospy.init_node('FirefighterPrototype', anonymous=True)

        # Variables initialisation
        self.android_cmd_list = []

        # Publishers
        self.pub_wheel_base_cmd = rospy.Publisher('/wheel_base_cmd', Int16MultiArray, queue_size=1)
        self.pub_turret_cmd = rospy.Publisher('/turret_cmd', Int16MultiArray, queue_size=1)
        self.pub_shoot_cmd = rospy.Publisher('/shoot_cmd', Bool, queue_size=1)

        # Subscribers
        rospy.Subscriber('/android_cmd', Byte, self.piloting)

    
    def piloting(self, cmd):
        if cmd.data == -1:
            self.android_cmd_list = []

        elif cmd.data == -2:
            # Parsing data coming from Android/ESP32
            piloting_wheel_base = Int16MultiArray()
            piloting_turret = Int16MultiArray()
            piloting_wheel_base.data = self.android_cmd_list[:4]
            piloting_turret.data = self.android_cmd_list[4:8]
            stop_wheel_base = self.android_cmd_list[8]
            piloting_wheel_base.data.append(stop_wheel_base)
            stop_turret = self.android_cmd_list[9]
            piloting_turret.data.append(stop_turret)
            piloting_shoot = bool(self.android_cmd_list[10])

            # Publishing the right parsed command to the corresponding component 
            self.pub_wheel_base_cmd.publish(piloting_wheel_base)
            self.pub_turret_cmd.publish(piloting_turret)
            self.pub_shoot_cmd.publish(piloting_shoot)

        else:
            if cmd.data < 0:
                cmd.data = cmd.data + 2**8 # unsigned = signed + 2**(bit number)
            self.android_cmd_list.append(cmd.data)


if __name__ == '__main__':
    try:
        firefighter_prototype = FirefighterPrototype()
        rospy.spin()
    except Exception as e:
        rospy.loginfo(str(e))
