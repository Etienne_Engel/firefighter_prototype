#!/usr/bin/env python3

import rospy
from std_msgs.msg import Byte, Int16MultiArray, Bool
from ax12 import Ax12


class Turret():

    def __init__(self, pad_values=None):
        """
        Input:
            pad_values (list) =
        """
        rospy.init_node('Turret', anonymous=True)

        if pad_values != None:
            self.x_pad_init = pad_values[0]
            self.y_pad_init = pad_values[1]
            self.x_pad_final = pad_values[2]
            self.y_pad_final = pad_values[3]
            if pad_values[4] == 1:
                self.stop = True
            else:
                self.stop = False
        Ax12.connect()
        self.dx1 = Ax12(1)
        self.dx1.set_moving_speed(200)
        self.dx2 = Ax12(2)
        self.dx2.set_moving_speed(200)

        # Subscribers
        rospy.Subscriber('/turret_cmd', Int16MultiArray, self.piloting)

    def set_pad_values(self, new_pad_values):
        self.x_pad_init = new_pad_values[0]
        self.y_pad_init = new_pad_values[1]
        self.x_pad_final = new_pad_values[2]
        self.y_pad_final = new_pad_values[3]
        if new_pad_values[4] == 1:
            self.stop = True
        else:
            self.stop = False

    def move_yaw(self, yaw_move):
        if not isinstance(yaw_move, int):
            return
        current_yaw_position = self.dx1.get_present_position()
        if current_yaw_position+yaw_move > 961:
            self.dx1.set_goal_position(961)
        elif current_yaw_position+yaw_move < 679:
            self.dx1.set_goal_position(679)
        else:
            self.dx1.set_goal_position(current_yaw_position+yaw_move)

    def move_roll(self, roll_move):
        if not isinstance(roll_move, int):
            return
        current_roll_position = self.dx2.get_present_position()
        if current_roll_position+roll_move > 961:
            self.dx2.set_goal_position(961)
        elif current_roll_position+roll_move < 679:
            self.dx2.set_goal_position(679)
        else:
            self.dx2.set_goal_position(current_roll_position+roll_move)

    def two_axis_move(self, roll_move, yaw_move):
        self.move_roll(roll_move)
        self.move_yaw(yaw_move)

    def move(self):
        # Calculate the user's deplacement
        x_android_app_move = self.x_pad_final - self.x_pad_init
        turret_yaw_move = int(x_android_app_move * 282 / 254)
        y_android_app_move = self.y_pad_final - self.y_pad_init
        turret_roll_move = int(y_android_app_move * 282 / 254)
        # Move turret according to user's deplacement
        self.two_axis_move(turret_roll_move, turret_yaw_move)

    def piloting(self, piloting_turret):
        xy_pad_values = list(piloting_turret.data)
        self.set_pad_values(xy_pad_values)
        if not self.stop:
            self.move()


if __name__ == '__main__':
    try:
        turret = Turret()
        rospy.spin()
    except Exception as e:
        rospy.loginfo(str(e))
